var FileSystem = require("fs");
var Path = require("path");
var Colors = require("colors"); // https://www.npmjs.com/package/colors

var Helper = require("./Helper");

var ARGUMENTS = require("minimist")(process.argv.slice(2));

function getApplicationId () {
	var pwd = process.env["PWD"].split("/");
	return pwd[pwd.length - 1].split("-").slice(1).join("-");
}

function getApplicationConfig (applicationId) {
	var applicationConfig = require("./applications/" + applicationId + "/config");
	if (!applicationConfig) {
		console.error("Missing application config file".red);
		process.exit(0);
	}
	
	return applicationConfig;
}

function copyFiles () {
	
	var applicationId = getApplicationId();
	var applicationConfig = getApplicationConfig(applicationId);

	var topDir = Path.resolve(Path.dirname(__filename), "..");
	
	//var appTargetDir = Path.resolve(topDir, "app/target/eventtravelprojects.guide.mobileapp-1.0/");
	var appWebDir = Path.resolve(topDir, "app/src/main/webapp/");
	
	// katalog, w którym znajduje się skompilowany projekt gwt
	// tylko jeśli kopiujemy pliki Eclipse'a
	var appGwtDir = Path.resolve(topDir, "app/target/compiled");

	var cordovaWWWDir = Path.resolve(process.cwd(), "www");
	
	console.log("Deleting cordova/www files");
	Helper.clearDir(cordovaWWWDir);

	console.log("Copying new files to cordova/www");
	[appWebDir, appGwtDir].forEach(function (dir) {
		
		var files = FileSystem.readdirSync(dir);
		for (var i = 0; i < files.length; i++) {
			var file = Path.resolve(dir, files[i]);
			
			if (files[i].indexOf(".") === 0 || files[i] == "META-INF" || files[i] == "WEB-INF") {
				continue;
			}
			
			if (FileSystem.statSync(file).isFile()) {
				Helper.copyFileSync(file, Path.resolve(cordovaWWWDir, files[i]));
			} else {
				Helper.copyFolderRecursiveSync(file, Path.join(cordovaWWWDir, files[i]));
			}
		}
	});
	
	console.log("Copying application res files to cordova/www");
	Helper.copyFolderRecursiveSync(Path.resolve(topDir, "cordova/applications/" + applicationId + "/res"), Path.join(cordovaWWWDir, "res"));
	
}

function updateIndex () {
	
	var applicationId = getApplicationId();
	var applicationConfig = getApplicationConfig(applicationId);
	
	var topDir = Path.resolve(Path.dirname(__filename), "..");
	var indexFile = Path.resolve(process.env["PWD"], "www/index.html");

	// make changes in index.html
	console.log("Modyfing index.html...");

	var index = FileSystem.readFileSync(indexFile, {encoding: "utf8"});
	index = index.replace(/<title>(.+?)<\/title>/g, "<title>" + applicationConfig["name"] + "</title>");
	
	if (applicationConfig["themeVariables.splashscreenBackgroundColor"]) {
		index = index.replace(/var SPLASHSCREEN_BACKGROUND = "(.+?)";/g, "var SPLASHSCREEN_BACKGROUND = \"" + applicationConfig["themeVariables.splashscreenBackgroundColor"] + "\";");
	}
	
	FileSystem.writeFileSync(indexFile, index);
	
}

module.exports = function (context) {
	
	copyFiles();
	updateIndex();
}