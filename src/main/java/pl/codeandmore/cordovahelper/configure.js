#!/usr/bin/env node

var FileSystem = require("fs");
var Path = require("path");
var Colors = require("colors"); // https://www.npmjs.com/package/colors

var Helper = require("./helper");


var CONFIGURATIONS = {
	"dist": "Build production app for store distribution", 
	"prod": "Build production app", 
	"prod-local": "Build production app with local backend", 
	"dev": "Build development app with production backend", 
	"dev-local": "Build development app with local backend"
};

/**
 * @see https://www.npmjs.com/package/minimist
 */
var ARGUMENTS = require("minimist")(process.argv.slice(2));


if (!ARGUMENTS["configuration"] || !CONFIGURATIONS[ARGUMENTS["configuration"]]) {
	console.error("Parameter ".red + "'configuration'".red.bold + " is missing or value invalid. Allowed values: ".red + JSON.stringify(CONFIGURATIONS, null, 2).green);
	process.exit(0);
}


if (!ARGUMENTS["application"]) {
	console.error("Parameter ".red + "'application'".red.bold + " is missing or value invalid.".red);
	process.exit(0);
}

console.log("Making configuration ".magenta + ARGUMENTS["configuration"].magenta.bold + "/" + ARGUMENTS["application"] + ":".magenta);


var rootDir = Path.resolve(Path.dirname(__filename), "..");
var cordovaDir = Path.resolve(rootDir, "cordova");
var templateDir = Path.resolve(cordovaDir, "template");
var applicationConfigDir = Path.resolve(cordovaDir, "applications/" + ARGUMENTS["application"]);
var configurationDir = Path.resolve(cordovaDir, "configurations", ARGUMENTS["configuration"] + "-" + ARGUMENTS["application"]);
var configFile = Path.resolve(configurationDir, "config.xml");
var wwwDir = Path.resolve(configurationDir, "www");

var NO_CLEAR = false, NO_COPY = false;

// prepare configuration dir
if (!NO_CLEAR) {
	
	var stat;
	try {
		stat = FileSystem.statSync(configurationDir);
	} catch (e) {
	}
	
	console.log("\tClearing...".green);
	
	if (stat && !stat.isDirectory()) {
		FileSystem.unlinkSync(configurationDir);
		
	} else if (stat && stat.isDirectory()) {
		Helper.clearDir(configurationDir);
		
	} else {
		FileSystem.mkdirSync(configurationDir);
	}
}

// copy from template
if (!NO_COPY) {
	console.log("\tCopying from template...".green);
	Helper.copyFolderRecursiveSync(templateDir, configurationDir);
}

var applicationConfig = require("./applications/" + ARGUMENTS["application"] + "/config");
if (!applicationConfig) {
	console.error("Missing application config file".red);
	process.exit(0);
}

// make changes in config.xml
if (!NO_CLEAR) {
	
	var cordovaConfig = FileSystem.readFileSync(configFile, {encoding: "utf8"});
	
	console.log("\tModyfing config.xml...".green);
	
	cordovaConfig = cordovaConfig.replace(/(<widget.+id=")(.+?)"/g, "$1" + applicationConfig["id"] + "\"");
	cordovaConfig = cordovaConfig.replace(/<name>(.+?)<\/name>/g, "<name>" + applicationConfig["name"] + "</name>");
	cordovaConfig = cordovaConfig.replace(/<description>(.+?)<\/description>/g, "<description>" + applicationConfig["description"] + "</description>");
	
	if (ARGUMENTS["configuration"] != "dist" && ARGUMENTS["configuration"] != "prod") {
		cordovaConfig = cordovaConfig.replace(/(<widget.+id=")(.+?)"/g, "$1$2-dev\"");
		cordovaConfig = cordovaConfig.replace(/<name>(.+?)<\/name>/g, "<name>$1-dev</name>");
	}
	
	FileSystem.writeFileSync(configFile, cordovaConfig);
}

// if dev fix launcher icon
if (!NO_CLEAR && ARGUMENTS["configuration"] != "dist" && ARGUMENTS["configuration"] != "prod") {
	
}
