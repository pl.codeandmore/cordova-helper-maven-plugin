package pl.codeandmore.cordovahelper;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

@Mojo(name = "configureApplication")
public class ConfigureApplicationMojo extends AbstractMojo {

	@Parameter(defaultValue = "${project}", required = true, readonly = true)
	MavenProject project;

	@Parameter(property = "cordovahelper.configurationType")
	ConfigurationType configurationType;

	@Parameter(property = "cordovahelper.application")
	String application;

	@Override
	public void execute () throws MojoExecutionException, MojoFailureException {
	}

}
