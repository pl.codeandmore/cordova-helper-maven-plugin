package pl.codeandmore.cordovahelper;


public enum ConfigurationType {
	
	/**
	 * Build production app for store distribution.
	 */
	DIST,
	
	/**
	 * Build production app.
	 */
	PROD,
	
	/**
	 * Build production app with local backend.
	 */
	PROD_LOCAL,
	
	/**
	 * Build production app with development backend.
	 */
	PROD_DEV,
	
	/**
	 * Build development app with development backend.
	 */
	DEV,
	
	/**
	 * Build development app with production backend.
	 */
	DEV_PROD,
	
	/**
	 * Build development app with local backend.
	 */
	DEV_LOCAL
	
}
