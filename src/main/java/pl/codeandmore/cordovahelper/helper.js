var FileSystem = require("fs");
var Path = require("path");

function copyFileSync (source, target) {
	FileSystem.writeFileSync(target, FileSystem.readFileSync(source));
}

function copyFolderRecursiveSync (source, target) {

	if (!FileSystem.existsSync(target)) {
		FileSystem.mkdirSync(target);
	}

	// copy
	if (FileSystem.lstatSync(source).isDirectory()) {
		
		FileSystem.readdirSync(source).forEach(function (fileName) {
			
			var file = Path.join(source, fileName);
			
			if (FileSystem.lstatSync(file).isDirectory()) {
				copyFolderRecursiveSync(file, Path.join(target, fileName));
			} else {
				copyFileSync(file, Path.join(target, fileName));
			}
			
		});
	}
}

function clearDir (dir) {

	var files;

	try {
		files = FileSystem.readdirSync(dir);
	} catch (e) {
		return;
	}

	if (files && files.length > 0) {
		for (var i = 0; i < files.length; i++) {
			var file = Path.resolve(dir, files[i]);
			if (FileSystem.statSync(file).isFile()) {
				FileSystem.unlinkSync(file);
			} else {
				clearDir(file);
				FileSystem.rmdirSync(file);
			}
		}
	}
}

function createDirIfNotExists (path) {
	
	var stat;
	try {
		stat = FileSystem.statSync(configurationDir);
	} catch (e) {
	}
	
	if (stat && !stat.isDirectory()) {
		throw "Cannot create directory - there is a file named like request directory: " + path
		
	} else if (stat && stat.isDirectory()) {
		
	} else {
		FileSystem.mkdirSync(path);
	}
}

module.exports = {
	copyFileSync: copyFileSync,
	copyFolderRecursiveSync: copyFolderRecursiveSync,
	clearDir: clearDir,
	createDirIfNotExists: createDirIfNotExists
}